﻿module CalculatorHelpers

    open System


    // operator, operand and parenthesis identifiers

    let operators = [| "*"; "/"; "+"; "-"; |]

    let negOperators = [| "*-"; "/-"; "+-"; "--"; |]

    let numericEx = System.Text.RegularExpressions.Regex("^[\.0-9]+$")

    let operandEx = System.Text.RegularExpressions.Regex("[\.0-9]+$")

    let isParen (s : string) : bool =
        s = "(" || s = ")"

    let isOperator (s : string) : bool =
        Array.exists (fun op -> op = s) operators

    let isNegOperator (s : string) : bool =
        Array.exists (fun nop -> nop = s) negOperators

    let isNegative (s : string) : bool =
        s = "-"

    let isNumeric (s : string) : bool =
        numericEx.IsMatch s


    // precedence and token helpers

    let appendResult (results : List<string>) (s : string) : List<string> =
        List.append results [ s; ]

    let isOperandToken (s : string) : bool =
        operandEx.IsMatch s

    let isLeftParenToken (s : string) : bool =
        s = "("

    let isRightParenToken (s : string) : bool =
        s = ")"

    let getOperatorPrecedence (op : string) : int =
        match op with
        | "*" | "*-" | "/" | "/-" -> 2
        | "+" | "+-" | "-" | "--" -> 1
        | _ -> 0

    let isOperatorToken (s : string) : bool =
        getOperatorPrecedence s > 0

    let stackTopIsNotLeftParen (st : Stack.Stack<string>) : bool =
        not (isLeftParenToken (Stack.top st))

    let precedenceLowerOrEqual (l : string) (r : string) : bool =
        (getOperatorPrecedence l) <= (getOperatorPrecedence r)
    

    // tokenizer

    let tokanizeNumericExpression (expression : string) : List<string> =
        let length = String.length expression

        let appendNegativeMultiplier (results : List<string>) : List<string> =
            List.append results [ "-1.0"; "*"; ]

        let rec tokenize (results : List<string>) (current : string) (index : int) : List<string> =
            if index >= length then
                if current <> "" then
                    appendResult results current
                else
                    results
            else
                let next = expression.[index].ToString ()
                if current = "" then
                    if isNegative next then
                        tokenize (appendNegativeMultiplier results) "" (index + 1)
                    else
                        tokenize results next (index + 1)
                else if isNumeric current then
                    if isNumeric next then
                        tokenize results (current + next) (index + 1)
                    else
                        tokenize (appendResult results current) next (index + 1)
                else if isOperator current then
                    if isNumeric next || isParen next then
                        tokenize (appendResult results current) next (index + 1)
                    else if (isNegative next) then
                        tokenize (appendResult results (current + next)) "" (index + 1)
                    else
                        tokenize results current (index + 1)
                else if isParen current then
                    if (isLeftParenToken current) && (isNegative next) then
                        tokenize (appendNegativeMultiplier (appendResult results current)) "" (index + 1)
                    else
                        tokenize (appendResult results current) next (index + 1)
                else
                    tokenize results current (index + 1)

        tokenize [] "" 0


    // convert infix tokens to postfix tokens

    let infixToPostfix (infix : List<string>) : List<string> =
        let mutable stack = Stack.StackContents List<string>.Empty
        let mutable results = List<string>.Empty
        
        let rec processInfix (segment : List<string>) : unit =
            match segment with
            | head :: tail ->
                if isOperandToken head then
                    results <- appendResult results head
                    processInfix tail
                else if isLeftParenToken head then
                    stack <- Stack.push stack head
                    processInfix tail
                else if isRightParenToken head then
                    let loopFunc () : bool =
                        (Stack.isNotEmpty stack) &&
                        (stackTopIsNotLeftParen stack)
                
                    let mutable loop = loopFunc ()
                
                    while loop do
                        results <- appendResult results (Stack.top stack)
                        stack <- Stack.pop stack
                        loop <- loopFunc ()

                    if (Stack.isNotEmpty stack) then
                        stack <- Stack.pop stack
                    else
                        ()

                    processInfix tail
                else
                    let loopFunc () : bool =
                        (Stack.isNotEmpty stack) &&
                        (precedenceLowerOrEqual head (Stack.top stack))
                
                    let mutable loop = loopFunc ()

                    while loop do
                        results <- appendResult results (Stack.top stack)
                        stack <- Stack.pop stack
                        loop <- loopFunc ()
                    
                    stack <- Stack.push stack head

                    processInfix tail
            | _ ->
                let mutable lastLoop = (Stack.isNotEmpty stack)

                while lastLoop do
                    results <- appendResult results (Stack.top stack)
                    stack <- Stack.pop stack
                    lastLoop <- (Stack.isNotEmpty stack)

        processInfix infix

        results


    // postfix to result

    let postfixToResult (postfix : List<string>) : string =
        let mutable stack = Stack.StackContents List<string>.Empty

        let getDecimalResult
            (d1 : double)
            (d2 : double)
            (op : string)
            : double =

            match op with
            | "*" ->
                d1 * d2
            | "*-" ->
                d1 * -d2
            | "/" ->
                d1 / d2
            | "/-" ->
                d1 / -d2
            | "+" ->
                d1 + d2
            | "+-" ->
                d1 + -d2
            | "-" ->
                d1 - d2
            | "--" ->
                d1 + d2
            | _ ->
                0.0

        let getResultOfOperation
            (operand1 : string)
            (operand2 : string)
            (operator : string)
            : string =

            let d1 = Convert.ToDouble(operand1)
            let d2 = Convert.ToDouble(operand2)

            (getDecimalResult d1 d2 operator).ToString ()

        let rec processPostfix (segment : List<string>) : unit =
            match segment with
            | head :: tail ->
                if isOperandToken head then
                    stack <- Stack.push stack head
                    processPostfix tail
                else // head is operator
                    let top = Stack.top stack
                    stack <- Stack.pop stack

                    if (Stack.isNotEmpty stack) then
                        let secondToTop = Stack.top stack
                        stack <- Stack.pop stack
                        stack <- Stack.push stack (getResultOfOperation secondToTop top head)
                    else
                        stack <- Stack.push stack top

                    processPostfix tail
            | _ ->
                ()

        processPostfix postfix

        if (Stack.isNotEmpty stack) then
            Stack.top stack
        else
            ""


    // validators

    let whiteListEx = System.Text.RegularExpressions.Regex("^[0-9\.\+\-\*\/\(\)]+$")

    let isWhiteListable (expression : string) : bool =
        whiteListEx.IsMatch expression

    let balancedParens (expression : string) : bool =
        let getNextStack (c : char) (stack : Stack.Stack<char>) =
            match c with
            | '(' ->
                Stack.push stack c
            | ')' ->
                if (Stack.isNotEmpty stack) && (Stack.top stack) = '(' then
                    Stack.pop stack
                else
                    Stack.push stack c
            | _ ->
                stack

        let rec scanExpression (chars : List<char>) (stack : Stack.Stack<char>) : bool =
            match chars with
            | head :: tail ->
                scanExpression tail (getNextStack head stack)
            | _ ->
                Stack.isEmpty stack

        (scanExpression (Seq.toList expression) (Stack.getEmpty ()))


    // infix to result

    let infixToResult (infix : string) : string =
        if not (isWhiteListable infix) then
            "contains invalid characters"
        else if not (balancedParens infix) then
            "parenthesis are not balanced"
        else
            let tokens = tokanizeNumericExpression infix
            let postfix = infixToPostfix tokens
            postfixToResult postfix
