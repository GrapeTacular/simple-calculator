﻿open System;

let stripSpaces (rawExpression : string) : string =
    (Seq.toList rawExpression)
    |> List.map (fun c -> c.ToString ())
    |> List.filter (fun c -> c <> " ")
    |> String.concat ""    


[<EntryPoint>]
let main argv =
    try
        let input = stripSpaces argv.[0]

        if input = "test" then
            Tests.testOfInvalidInfixToResult ()
            Tests.testOfValidInfixToResult ()
        else
            let result = CalculatorHelpers.infixToResult input
            printfn "%s\n" result
    with
    | :? Exception as ex ->
        printfn "%s" (ex.ToString ())

    0 // return an integer exit code