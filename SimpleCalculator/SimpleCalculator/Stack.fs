﻿module Stack

    type Stack<'t> = StackContents of 't list

    let push<'t> (stack : Stack<'t>) (t : 't) : Stack<'t> =
        let (StackContents contents) = stack
        let newContents = t :: contents
        StackContents newContents

    let getEmpty<'t> () : Stack<'t> =
        StackContents []

    let pop<'t> (stack : Stack<'t>) : Stack<'t> =
        let (StackContents contents) = stack
        match contents with
        | t :: ts -> StackContents ts
        | _ -> getEmpty ()

    let isEmpty<'t> (stack : Stack<'t>) : bool =
        let (StackContents contents) = stack
        match contents with
        | t :: ts -> false
        | _ -> true

    let isNotEmpty<'t> (stack : Stack<'t>) : bool =
        not (isEmpty stack)

    let top<'t> (stack : Stack<'t>) : 't =
        let (StackContents contents) = stack
        match contents with
        | t :: ts -> t
        | _ -> failwith "stack is empty"
