﻿module Tests

    let testOfInvalidInfixToResult () : unit =
        let test = "-12.34^-(-56.78+-0.9)"
        printfn "%s" test
        let result = CalculatorHelpers.infixToResult test
        printfn "%s\n" result
        let test2 = "-((-56.78--0.9)--(12-3.4)"
        printfn "%s" test2
        let result2 = CalculatorHelpers.infixToResult test2
        printfn "%s\n" result2
        let test3 = "-(51.2*-(5-4.1)))"
        printfn "%s" test3
        let result3 = CalculatorHelpers.infixToResult test3
        printfn "%s\n" result3

    let testOfValidInfixToResult () : unit =
        let test = "-12.34*-(-56.78+-0.9)"
        printfn "%s" test
        let result = CalculatorHelpers.infixToResult test
        printfn "%s" result
        printfn "valid: %b\n" (result = "-711.7712")
        let test2 = "-(-56.78--0.9)--(12-3.4)"
        printfn "%s" test2
        let result2 = CalculatorHelpers.infixToResult test2
        printfn "%s" result2
        printfn "valid: %b\n" (result2 = "64.48")
        let test3 = "-(51.2*-(5-4.1))"
        printfn "%s" test3
        let result3 = CalculatorHelpers.infixToResult test3
        printfn "%s" result3
        printfn "valid: %b\n" (result3 = "46.08000000000002")
        let test4 = "-(((-56.78)--(0.9)))-(-12)"
        printfn "%s" test4
        let result4 = CalculatorHelpers.infixToResult test4
        printfn "%s" result4
        printfn "valid: %b\n" (result4 = "67.88")
        let test5 = "-(((-56.78)))"
        printfn "%s" test5
        let result5 = CalculatorHelpers.infixToResult test5
        printfn "%s" result5
        printfn "valid: %b\n" (result5 = "56.78")
